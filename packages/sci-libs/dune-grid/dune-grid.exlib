# Copyright 2010 Elias Pipping <pipping@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require flag-o-matic

export_exlib_phases pkg_setup src_install

SUMMARY="Module for grids in DUNE, the modular toolbox for solving partial differential equations"
HOMEPAGE="http://www.dune-project.org"

LICENCES="dune"
SLOT="0"
MYOPTIONS="X"

DEPENDENCIES="
    build+run:
        dev-libs/gmp:=
        sci-libs/ALUGrid[>=1.50]
        sci-libs/dune-geometry[~${PV}]
        sci-libs/dune-common[~${PV}]
        virtual/blas
        virtual/lapack
        X? ( x11-libs/libX11 )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-shared
    --with-alugrid=/usr

    --without-alberta
    --without-alglib
    --without-amiramesh
    --without-grape
    --without-psurface
    --without-ug
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'X x'
)

dune-grid_pkg_setup() {
    append-flags -fno-strict-aliasing
}

dune-grid_src_install() {
    default

    # TODO: The share dir looks rather messy
    edo rmdir "${IMAGE}"/usr/share/doc/${PNVR}/doxygen

    insinto /usr/include/dune/grid
    doins -r dune/grid/test
}

